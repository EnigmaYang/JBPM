package cn.yawei.decision;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class DecisionHandler3 implements DecisionHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8283685029914123939L;

	@Override
	public String decide(OpenExecution execution) {
		int days = (Integer) execution.getVariable("days");

		if (days < 2)
			return "lt2";
		else if (days > 10)
			return "gt10";
		else
			return "gt2_lt10";
	}

}
