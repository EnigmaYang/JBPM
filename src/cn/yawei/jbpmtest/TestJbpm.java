package cn.yawei.jbpmtest;

import java.util.List;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Task;
import org.junit.Before;
import org.junit.Test;

public class TestJbpm {
	ProcessEngine processEngine = null;

	@Before
	public void init() {
		processEngine = Configuration.getProcessEngine();
	}

	// deploy a process
	@Test
	public void deploy() {

		RepositoryService repositoryService = processEngine.getRepositoryService();
		repositoryService.createDeployment().addResourceFromClasspath("test.jpdl.xml").deploy();
	}

	// create a instance
	@Test
	public void createInstance() {

		ExecutionService executionService = processEngine.getExecutionService();
		ProcessInstance processInstance = executionService.startProcessInstanceByKey("test");
		System.out.println("process instance ID===" + processInstance.getId());
	}

	// get the tast from currect user
	@Test
	public void getTask() {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.findPersonalTasks("sigma");
		Task task = tasks.get(0);
		System.out.println("tasks numbers===" + tasks.size());
		System.out.println("task for name===" + task.getActivityName());
		System.out.println("task for user===" + task.getAssignee());
		System.out.println("task get ID=====" + task.getId());
	}

	// get the node where is it
	@Test
	public void getCurrectActivity() {

		ExecutionService executionService = processEngine.getExecutionService();
		String activityName = executionService.createProcessInstanceQuery().processInstanceId("test.110007")
				.uniqueResult().findActiveActivityNames().toString();
		
		String status = executionService.createProcessInstanceQuery().processInstanceId("test.110007")
				.uniqueResult().toString();
		
		System.out.println("current task is in node====="+activityName);
		System.out.println("current task status========="+status);
	}

	// complete the task
	@Test
	public void completeTask() {
		TaskService taskService = processEngine.getTaskService();
		taskService.completeTask("20001");
	}
}
