package cn.yawei.jbpmtest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Task;
import org.junit.Test;

public class TestVariable implements JbpmUtil {
	protected RepositoryService repositoryService; // 部署流程服务

	protected ExecutionService executionService; // 流程执行服务

	protected TaskService taskService;// 任务服务

	protected HistoryService historyService;// 任务服务

	protected ManagementService managementService;// 任务服务

	protected IdentityService identityService;// 任务服务

	@Override
	@Test
	public void createInstance() {
		deploy();
		Map<String, Object> variable = new HashMap<String, Object>();
		variable.put("UserId", "001");
		variable.put("userName", "tom");
		ProcessInstance processInstance = executionService.startProcessInstanceByKey("test", variable);

		print("流程实例ID", processInstance.getId());
	}

	@Test
	public void getMyVariable() {
		deploy();
		String userId = executionService.getVariable("test.50001", "UserId").toString();
		String userName = executionService.getVariable("test.50001", "userName").toString();
		print("UserID", userId);
		print("userName", userName);
	}

	@Test
	public void updateMyVariable() {
		deploy();
		executionService.setVariable("test.50001", "UserId", "123456");
		executionService.setVariable("test.50001", "userName", "zzzzzzzzzzzz");
		getMyVariable();

	}

	@Test
	public void deleteMyVariable() {
		// 删除实在流程监听的时候才能删除，这里只能添加修改。

	}

	@Test
	public void findAllMyVariable() {
		deploy();
		Set<String> set = executionService.getVariableNames("test.50001");
		Iterator i = set.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}

		Map<String, Object> map = executionService.getVariables("test.50001", set);
		Iterator i2 = map.entrySet().iterator();
		while (i2.hasNext()) {
			Map.Entry m = (Entry) i2.next();
			print(m.getKey().toString(), m.getValue().toString());
		}

	}

	@Override
	@Test
	public void getCurrentActivity() {
		deploy();
		String activityName = executionService.createProcessInstanceQuery().processInstanceId("test.30001")
				.uniqueResult().findActiveActivityNames().toString();

		String status = executionService.createProcessInstanceQuery().processInstanceId("test.30001").uniqueResult()
				.toString();

		print("activityName", activityName);
		print("status", status);

	}

	@Override
	@Test
	public void getTask() {
		deploy();
		List<Task> tasks = taskService.findPersonalTasks("enigma");
		Task task = tasks.get(0);
		System.out.println("tasks numbers===" + tasks.size());
		System.out.println("task for name===" + task.getActivityName());
		System.out.println("task for user===" + task.getAssignee());
		System.out.println("task get ID=====" + task.getId());
	}

	@Test
	public void addTaskVariable() {
		deploy();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("taskUserID", "100");
		map.put("taskUserName", "jackson");
		taskService.setVariables("50004", map);
		getTaskVariable();
	}

	@Test
	public void getTaskVariable() {
		deploy();
		Set<String> set = taskService.getVariableNames("50004");
		Iterator i = set.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
		
		Map<String, Object> map = taskService.getVariables("50004", set);
		Iterator i2 = map.entrySet().iterator();
		while (i2.hasNext()) {
			Map.Entry m = (Entry) i2.next();
			print(m.getKey().toString(), m.getValue().toString());
		}

	}

	@Override
	@Test
	public void completeTask() {
		deploy();
		taskService.completeTask("40001");

	}

	public void deploy() {
		ProcessEngine processEngine = Configuration.getProcessEngine();
		repositoryService = processEngine.getRepositoryService();
		executionService = processEngine.getExecutionService();
		taskService = processEngine.getTaskService();
		historyService = processEngine.getHistoryService();
		managementService = processEngine.getManagementService();
		identityService = processEngine.getIdentityService();
	}

	public void print(String name, String value) {
		System.out.println(name + "==================" + value);
	}

}
