package cn.yawei.jbpmtest;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Task;
import org.junit.Test;

public class TestJUEL implements JbpmUtil {
	protected RepositoryService repositoryService; // 部署流程服务

	protected ExecutionService executionService; // 流程执行服务

	protected TaskService taskService;// 任务服务

	protected HistoryService historyService;// 任务服务

	protected ManagementService managementService;// 任务服务

	protected IdentityService identityService;// 任务服务

	@Override
	public void deploy() {
		ProcessEngine processEngine = Configuration.getProcessEngine();
		repositoryService = processEngine.getRepositoryService();
		executionService = processEngine.getExecutionService();
		taskService = processEngine.getTaskService();
		historyService = processEngine.getHistoryService();
		managementService = processEngine.getManagementService();
		identityService = processEngine.getIdentityService();
	}

	@Test
	public void testDeployNew() {
		deploy();
		repositoryService.createDeployment().addResourceFromClasspath("test.jpdl.xml").deploy();

	}

	public void print(String name, String value) {
		System.out.println(name + "==================" + value);
	}

	@Override
	@Test
	public void createInstance() {
		deploy();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("kezhang", "tom");
		map.put("zhuren", "jack");

		ProcessInstance pi = executionService.startProcessInstanceByKey("test", map);
		System.out.println(pi.getId());
	}

	@Override
	public void getCurrentActivity() {
		// TODO Auto-generated method stub

	}

	@Override
	@Test
	public void getTask() {
		deploy();
		Task task = taskService.findPersonalTasks("jack").get(0);
		System.out.println(task.getActivityName());
		System.out.println(task.getId());
		System.out.println(task.getName());
		System.out.println(task.getAssignee());
	}

	@Override
	@Test
	public void completeTask() {
		deploy();
		taskService.completeTask("100001");
		getTask();
	}

}
