package cn.yawei.task.candidate_group;

import java.util.List;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Task;
import org.junit.Before;
import org.junit.Test;

public class TestTaskCandidateGroup {
	private ExecutionService executionService;
	private HistoryService historyService;
	private IdentityService identityService;
	private ManagementService managementService;
	private RepositoryService repositoryService;
	private TaskService taskService;

	// Group Name: teamLead
	@Before
	public void initJBPM() {
		ProcessEngine processEngine = Configuration.getProcessEngine();
		executionService = processEngine.getExecutionService();
		historyService = processEngine.getHistoryService();
		identityService = processEngine.getIdentityService();
		managementService = processEngine.getManagementService();
		repositoryService = processEngine.getRepositoryService();
		taskService = processEngine.getTaskService();
	}

	@Test
	public void deploy() {

		String groupName = identityService.createGroup("tlead");
		identityService.createUser("UserID111", "UserName_1", "UserFamily_1111");
		identityService.createUser("UserID222", "UserName_2", "UserFamily_2222");

		identityService.createMembership("UserID111", groupName);
		identityService.createMembership("UserID222", groupName);

		repositoryService.createDeployment().addResourceFromClasspath("cn/yawei/task/candidate_group/task2.jpdl.xml")
				.deploy();

	}

	@Test
	public void createInstance() {
		ProcessInstance pocessInstance = executionService.startProcessInstanceByKey("task");
		System.out.println("任务ID:" + pocessInstance.getId());
	}

	@Test
	public void getCurrentActivity() {
		String name = executionService.createProcessInstanceQuery().processInstanceId("task.250001").uniqueResult()
				.toString();
		System.out.println("任务名称:" + name);
	}

	@Test
	public void getTask() {
//		taskService.takeTask("250002", "UserID111"); //从组里分配一个任务给UserID111这个人
		List<Task> list = taskService.findPersonalTasks("UserID111");
		System.out.println("任务数量:" + list.size());
		System.out.println("任务名称:" + list.get(0).getActivityName());
		System.out.println("任务ID:" + list.get(0).getId());
	}

	@Test
	public void getGroupTask() {
		List<Task> list = taskService.findGroupTasks("UserID111");// 这里注意的是不是groupName，还是要用UserID来获取
		System.out.println("任务数量:" + list.size());
		System.out.println("任务名称:" + list.get(0).getActivityName());
		System.out.println("任务ID:" + list.get(0).getId());
	}

	@Test
	public void completeTask() {
		taskService.completeTask("260002");
	}
}
