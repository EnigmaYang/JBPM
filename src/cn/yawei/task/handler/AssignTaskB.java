package cn.yawei.task.handler;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

public class AssignTaskB implements AssignmentHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6503972182394889830L;

	@Override
	public void assign(Assignable assignable, OpenExecution execution) throws Exception {
		assignable.setAssignee("UserB");
		System.out.println("Hello");
	}

}
