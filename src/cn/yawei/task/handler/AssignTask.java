package cn.yawei.task.handler;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

public class AssignTask implements AssignmentHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4988691189662327184L;

	@Override
	public void assign(Assignable assignable, OpenExecution execution) throws Exception {
		assignable.setAssignee("userA");
	}

}
