package cn.yawei.task.handler;

import java.util.List;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.api.task.Task;
import org.junit.Before;
import org.junit.Test;

public class TestTaskHandler {
	private ExecutionService executionService;
	private HistoryService historyService;
	private IdentityService identityService;
	private ManagementService managementService;
	private RepositoryService repositoryService;
	private TaskService taskService;

	@Before
	public void initJBPM() {
		ProcessEngine processEngine = Configuration.getProcessEngine();
		executionService = processEngine.getExecutionService();
		historyService = processEngine.getHistoryService();
		identityService = processEngine.getIdentityService();
		managementService = processEngine.getManagementService();
		repositoryService = processEngine.getRepositoryService();
		taskService = processEngine.getTaskService();
	}

	@Test
	public void deploy() {
		repositoryService.createDeployment().addResourceFromClasspath("cn/yawei/task/handler/task1.jpdl.xml").deploy();
	}

	@Test
	public void createInstance() {
		ProcessInstance pocessInstance = executionService.startProcessInstanceByKey("task");
		System.out.println("任务ID:" + pocessInstance.getId());
	}

	@Test
	public void getCurrentActivity() {
		String name = executionService.createProcessInstanceQuery().processInstanceId("task.170001").uniqueResult()
				.toString();
		System.out.println("任务名称:" + name);
	}

	@Test
	public void getTask() {
		// Task task = taskService.getTask(""); 
		List<Task> list = taskService.findPersonalTasks("UserA");
		System.out.println("任务数量:" + list.size());
		System.out.println("任务名称:" + list.get(0).getActivityName());
		System.out.println("任务ID:" + list.get(0).getId());
	}

	@Test
	public void completeTask() {
		taskService.completeTask("200002");
	}
}
