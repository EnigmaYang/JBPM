package cn.yawei.state;

import org.jbpm.api.Configuration;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.junit.Before;
import org.junit.Test;

public class TestState {

	private ExecutionService executionService;
	private HistoryService historyService;
	private IdentityService identityService;
	private ManagementService managementService;
	private RepositoryService repositoryService;
	private TaskService taskService;

	@Before
	public void initJBPM() {
		ProcessEngine ps = Configuration.getProcessEngine();
		executionService = ps.getExecutionService();
		historyService = ps.getHistoryService();
		identityService = ps.getIdentityService();
		managementService = ps.getManagementService();
		repositoryService = ps.getRepositoryService();
		taskService = ps.getTaskService();
	}

	@Test
	public void deploy() {
		repositoryService.createDeployment().addResourceFromClasspath("cn/yawei/state/state.jpdl.xml").deploy();
	}

	@Test
	public void createInstance() {
		ProcessInstance startProcessInstanceByKey = executionService.startProcessInstanceByKey("state");
		System.out.println("流程实例ID:" + startProcessInstanceByKey.getId());
	}

	@Test
	public void checkCurrectActivity() {
		String name = executionService.createProcessInstanceQuery().processInstanceId("state.130001").uniqueResult()
				.findActiveActivityNames().toString();
		System.out.println("流程当前节点名字:" + name);
	}

	@Test
	public void checkStateInformation() {
		executionService.signalExecutionById("state.130001");
		checkCurrectActivity();
	}

}
